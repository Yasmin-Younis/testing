package tst;
// tutorial from the Internet 
// Doesn't belong to the task 
// http://appsdeveloperblog.com/junit-test-expected-exception-example/

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
 
public class ExpectingExceptionTest {
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    @Test 
    public void testUsingTheRuleNullPointerException()
    {
        thrown.expect(NullPointerException.class);
        String name = getName();
        System.out.println(name.length());
    }
    private String getName() {
        return null;
    }
}