package tst;

import org.junit.Assert;
import org.junit.Test;

import testPatient.MergeUniqueArrays;

//import testPatient.Patient;

public class MergeTest {
	@Test
	public void ConstructorTest() {
		int[] intArray1 = { 1,4,5}; 
		int[] intArray2 = { 2,6,9}; 
		
		int[] testArray = { 1,2,4,5,6,9}; 


	int [] r = MergeUniqueArrays.mergeArrays(intArray1, intArray2);
	Assert.assertArrayEquals(r, testArray);
		
	}
	
	
}